Given("I Navigate end point post") do
  @response =  RestClient.get "https://www.mwtestconsultancy.co.uk/auth", content_type: :json, accept: :json
end

Given("I Navigate end point delete") do
  @response =  RestClient.get "https://www.mwtestconsultancy.co.uk/booking/1", content_type: :json, accept: :json
end
