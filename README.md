## How to run this project

1. Clone this repository.
2. Gem install bundler.
3. Gem install selenium-webdriver.
4. Gem install cucumber.
5. Gem install rspec.
6. Type cucumber in your project.